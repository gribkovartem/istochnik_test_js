var yql_url = 'https://query.yahooapis.com/v1/public/yql',
    email = 'gribkovartem@gmail.com',
    url = 'http://hires.istochnik.im/candles?email=' + email,
    json_data = $.ajax({
        async: false,
        'url': yql_url,
        'data': {
            'q': 'SELECT * FROM json WHERE url="'+url+'"',
            'format': 'json',
            'jsonCompat': 'new'
        },
        'dataType': 'json'
    }).responseJSON,
    data = json_data.query.results.json.data.candles.data,
    dates_list = [],
    open_list = [],
    close_list = [],
    high_list = [],
    low_list = [];

/**
 * Собираем данные для графиков
 */
data.forEach(function(element) {
    dates_list.push(element.json[6]);
    open_list.push(element.json[0]);
    close_list.push(element.json[1]);
    high_list.push(element.json[2]);
    low_list.push(element.json[3]);
});

/**
 * Инициализируем графики
 * @type {{animation: boolean, showTooltips: boolean}}
 */
var options = {
        animation: false,
        showTooltips: false
    },
    open_chart = document.getElementById('open').getContext('2d'),
    open_chart_data = {
        labels : dates_list,
        datasets : [
            {
                fillColor : "rgba(172,194,132,0.4)",
                strokeColor : "#ACC26D",
                pointColor : "#fff",
                pointStrokeColor : "#9DB86D",
                data : open_list
            }
        ]
    },
    close_chart = document.getElementById('close').getContext('2d'),
        close_chart_data = {
        labels : dates_list,
        datasets : [
            {
                fillColor : "rgba(172,194,132,0.4)",
                strokeColor : "#ACC26D",
                pointColor : "#fff",
                pointStrokeColor : "#9DB86D",
                data : close_list
            }
        ]
    },
    high_chart = document.getElementById('high').getContext('2d'),
    high_chart_data = {
        labels : dates_list,
        datasets : [
            {
                fillColor : "rgba(172,194,132,0.4)",
                strokeColor : "#ACC26D",
                pointColor : "#fff",
                pointStrokeColor : "#9DB86D",
                data : high_list
            }
        ]
    },
    low_chart = document.getElementById('low').getContext('2d'),
    low_chart_data = {
        labels : dates_list,
        datasets : [
            {
                fillColor : "rgba(172,194,132,0.4)",
                strokeColor : "#ACC26D",
                pointColor : "#fff",
                pointStrokeColor : "#9DB86D",
                data : low_list
            }
        ]
    },
    open_chart_var = new Chart(open_chart).Line(open_chart_data, options),
    close_chart_var = new Chart(close_chart).Line(close_chart_data, options),
    high_chart_var = new Chart(high_chart).Line(high_chart_data, options),
    low_chart_var = new Chart(low_chart).Line(low_chart_data, options);

/**
 * Скрыть или показать график
 */
$('.chart-toggle-view').on('click', function() {
    $(this).parents('.chart').find('canvas').toggle();
});

/**
 * Поменять тип графика
 */
$('.chart-toggle-type').on('click', function() {
    var chart = $(this).parents('.chart'),
        chart_id = chart.find('canvas').attr('id'),
        reinit_chart = function(chart_id) {
            var chart_vars;

            switch (chart_id) {
                case 'open':
                    chart_vars = {chart_name: open_chart, chart_data_name: open_chart_data, chart_var_name: open_chart_var};
                    break;
                case 'close':
                    chart_vars = {chart_name: close_chart, chart_data_name: close_chart_data, chart_var_name: close_chart_var};
                    break;
                case 'high':
                    chart_vars = {chart_name: high_chart, chart_data_name: high_chart_data, chart_var_name: high_chart_var};
                    break;
                case 'low':
                    chart_vars = {chart_name: low_chart, chart_data_name: low_chart_data, chart_var_name: low_chart_var};
                    break;
            }

            return chart_vars;
        },
        chart_vars = reinit_chart(chart_id);

    chart.toggleClass('bar');
    chart_vars.chart_var_name.destroy();
    if (chart.hasClass('bar')) {
        chart_vars.chart_var_name = new Chart(chart_vars.chart_name).Bar(chart_vars.chart_data_name, options);
    } else {
        chart_vars.chart_var_name = new Chart(chart_vars.chart_name).Line(chart_vars.chart_data_name, options);
    }
});